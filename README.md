# Description
Le site doit ressembler à la maquette et être responsive, utilisez donc un framework (Bootstrap, Zurb Foundation, ...)

Pour les icônes des réseaux sociaux en haut vous pouvez utiliser font-awesome : http://fontawesome.io/

La police principale est : "Roboto", Helvetica, Arial, sans-serif
https://fonts.google.com/specimen/Roboto
